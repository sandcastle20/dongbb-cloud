# dongbb-cloud

#### 介绍
Spring Cloud 微服务项目、权限管理、sentinel、nacos为核心。
[《实战SpringCloud微服务》](https://www.kancloud.cn/hanxt/springcloud/content) 在线电子书，配套源代码

#### 软件架构
软件架构说明

![输入图片说明](https://images.gitee.com/uploads/images/2022/0519/052027_a1359aa1_349604.png "屏幕截图.png")



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
